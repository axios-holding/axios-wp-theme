=== Axios Holding ===
Contributors: velezhanski
Tags: one-column, flexible-header, accessibility-ready, custom-colors, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, blog, axios
Requires at least: 4.9.6
Tested up to: WordPress 5.0
Requires PHP: 5.2.4
Stable tag: 0.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This is a custom theme developed for the Axios Holding Website.

== Description ==
This is a custom theme developed for the Axios Holding Website. It features custom styles for all the default blocks, and is built so that what you see in the editor looks like what you'll see on your website. Axios Holding is designed to be adaptable to a wide range of devices, be it a mobile or an ultra-wide monitor.

== Changelog ==

= 0.4 =
* Released: May 21, 2020

Alpha release

== Resources ==
* Fully integrated ACF
* Cleaned up the media
* Cleaned up the page structure

= 0.3 =
* Released: May 19, 2020

Alpha release

== Resources ==
* Fix up the structure
* Remove unwanted pages
* Fixed images
* Cleaned up the templates

= 0.2 =
* Released: May 13, 2020

Alpha release

== Resources ==
* Fix up the structure
* Remove unwanted assets
* Added support for branch commits

= 0.1 =
* Released: May 8, 2020

Alpha release

== Resources ==
* Initial release

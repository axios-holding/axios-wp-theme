<?php
/**
 *
 */?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php include "components/_styles.php";?>
  <title>Axios Holding - Search</title>
  <?php include "components/_metatags.php";?>
</head>

<body>
  <?php include "components/_header.php";?>
  <main id="search-results" class="axios-bg-light">
    <div class="container-fluid px-0 hero-container">
      <div class="row mx-0">
        <div class="col-12 px-0">
          <div class="hero-content-container"></div>
          <div
            class="hero-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
          </div>
        </div>
      </div>
    </div>
    <div class="main-content py-5 position-relative">
      <h2 class="axios-text-dark text-center underline underline-light inner-template-heading">search results</h2>
      <div id="blog-results">
        <div class="container articles-container">
          <div class="row pt-3 pt-lg-0">
            <?php
                // the query
                $the_query = new WP_Query(array(
                    'posts_per_page' => 100,
                ));
            ?>
            <?php if (have_posts()): ?>
            <?php while (have_posts()): the_post();?>
            <div class="col-12 col-sm-6 col-md-4 pb-5 article">
              <div class="mx-auto article-cont">
                <a href="<?php the_permalink();?>" class="text-center">
                  <div class="row mx-auto article-img-cont">
                    <div class="position-relative d-flex m-auto article-img-cont-in">
                      <div class="mx-auto bg-img">
                        <?php the_post_thumbnail('full', array('class' => 'img-fluid'));?></div>
                    </div>
                  </div>
                  <div class="row mx-auto article-date"><span
                      class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                  <div class="row mx-auto article-text"><span
                      class="col-9 col-12 mx-auto text"><?php the_title();?></span></div>
                </a>
              </div>
            </div>

            <?php endwhile;?>
            <?php wp_reset_postdata();?>

            <?php else: ?>
            <p><?php __('No Posts');?></p>
            <?php endif;?>
          </div>
        </div>
      </div>
      <div
        class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none">
      </div>
    </div>

  </main>

  <?php include "components/_footer.php";?>
  <?php include "components/_scripts.php";?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nice-select.min.js"></script>
  <script>
    $(window).on('load resize orientationchange', function () {
      if ($(window).width() >= 768) {
        $('select').niceSelect();
      } else {
        $('select').niceSelect('destroy');
      }
    });

    $(document).ready(function () {

      var cursor = document.querySelector(".custom-cursor");
      var select = document.querySelectorAll(".nice-select");
      var select_dropdown = document.querySelectorAll(".option");

      for (var i = 0; i < select.length; i++) {
        var selfSelect = select[i];
        selfSelect.addEventListener("mouseover", function () {
          cursor.classList.add("custom-cursor--link");
        });
        selfSelect.addEventListener("mouseout", function () {
          cursor.classList.remove("custom-cursor--link");
        });
      }

      for (var j = 0; j < select_dropdown.length; j++) {
        var selfSelect_dropdown = select_dropdown[j];
        selfSelect_dropdown.addEventListener("mouseover", function () {
          cursor.classList.add("custom-cursor--link");
        });
        selfSelect_dropdown.addEventListener("mouseout", function () {
          cursor.classList.remove("custom-cursor--link");
        });
      }


    });
    $(window).on('load ', function () {

      var about_us_scroll_ctrl = new ScrollMagic.Controller();

      var tweenPosts = new TimelineMax()
      tweenPosts.add([
        TweenMax.staggerFromTo("#blog-results .article", 0.4, {
          x: "-220px",
          opacity: '0'
        }, {
          ease: Power1.easeOut,
          x: 0,
          opacity: '1',
          delay: 0.8
        }, 0.15),
      ]);

      /***************
       * Scroll Reveal Animation
       **************/
      var tween_about_us_section_4 = new TimelineMax();
      tween_about_us_section_4.add([
        TweenMax.staggerFromTo("#page-results .page-cont", 0.4, {
          x: "-220px",
          opacity: '0'
        }, {
          ease: Power1.easeOut,
          x: 0,
          opacity: '1'
        }, 0.15),
      ]);
      var scene_about_us = new ScrollMagic.Scene({
        triggerElement: '#page-results',
        triggerHook: 'onEnter',
        offset: 0,
      });
      scene_about_us.setTween(tween_about_us_section_4);
      scene_about_us.addTo(about_us_scroll_ctrl);
      scene_about_us.reverse(true);

    });
  </script>
</body>

</html>
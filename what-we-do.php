<?php
/**
 * Template Name: what we do
 */?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php include("components/_styles.php"); ?>
  <style>
    .mx-auto.bg-img {
      background-size: contain !important;
    }

    #what-we-do .vertical-container .vertical-cont-inner .companies .text-cont ul {
      color: #fff;
      font-family: "Nunito Sans", sans-serif;
      font-size: 15px;
      font-weight: 400;
      line-height: 24px;
      margin-bottom: 30px;
    }
  </style>
  <title>Axios Holding - What We Do</title>
  <?php include("components/_metatags.php"); ?>
</head>

<body>
  <?php include("components/_header.php"); ?>
  <main id="what-we-do">
    <div class="position-relative">
      <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
          <div class="col-12 px-0">
            <div class="bg-img hero-bg">
              <img alt="about-header"
                src="/wp-content/themes/axios-wp-theme/assets/images/headers/aboutus-header_BG.jpg">
            </div>
            <div class="container-fluid pb-5">
              <div class="row">
                <div class="col-12 px-0 pb-5">
                  <div class="hero-content-container px-3">
                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">what
                      we do</h1>
                    <div class="content">
                      <h3 class="col-10 col-lg-12 mx-auto text-center">Powered by technology. Designed by people</h3>
                      <div class="col-12 col-md-8 pb-5 mx-auto text-center hero-text">
                        <p class="col-12 col-lg-5 px-0 mx-auto text-center axios-text-light">Axios Holding is a FinTech
                          incubator, fathering companies from a range of online verticals such as Education, Lending,
                          Payments, Brokerage & Liquidity, User Engagement and DevOps. Axios offers a comprehensive set
                          of FinTech products and solutions by either breeding its own ideas or partnering with other
                          companies in related verticals. </p>
                      </div>
                    </div>
                  </div>
                  <!-- Particle Canvas Container -->
                  <div id="particle-canvas" class="text-center position-relative bg-img canvas-container">
                    <img alt="particles-mobile"
                      src="/wp-content/themes/axios-wp-theme/assets/images/whatwedo_particles_mobile.png">
                    <div class="verticals-list">
                      <img id="particlelogo" class="next-particle" alt="logo" data-particle-gap="5"
                        data-mouse-force="-10"
                        src="/wp-content/themes/axios-wp-theme/assets/images/Hero_particle_2.png">
                      <div id="hotspot-container" class="pb-5 btn-container">
                        <div class="hotspot-btn">
                          <!-- <a href="/" class="py-4 py-md-0 button left" id="online-gambling-btn"
                            data-slide="online-gambling">User Engagement Platforms</a> -->
                          <a href="/" class="py-4 py-md-0 button" id="education-platforms-btn"
                            data-slide="education-platforms">Education Platforms</a>
                          <a href="/" class="py-4 py-md-0 button left" id="technology-providers-btn"
                            data-slide="technology-providers">Technology Providers</a>
                          <!--<a href="/" class="py-4 py-md-0 button left" id="physical-payments-btn" data-slide="physical-payments">Payment Aggregator</a>-->
                          <a href="/" class="py-4 py-md-0 button" id="trading-platforms-btn"
                            data-slide="trading-platforms">Investment Platforms</a>
                          <a href="/" class="py-4 py-md-0 button left" id="online-payments-btn"
                            data-slide="online-payments">Online Payments</a>
                          <a href="/" class="py-4 py-md-0 button" id="online-lending-btn"
                            data-slide="online-lending">Online Lending</a>
                          <a href="/" class="py-4 py-md-0 button" id="exclusive-partners-btn"
                            data-slide="exclusive-partners">Exclusive Partners</a>
                          <a href="/" class="py-4 py-md-0 button left" id="kyc-btn" data-slide="kyc">KYC & ID Verification</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /Particle Canvas Container End -->
                </div>
              </div>
            </div>
            <div
              class="about-us-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
            </div>
          </div>
        </div>
      </div>
      <!-- Verticals -->
      <div id="online-gambling" class="container-fluid mx-auto vertical-container">
        <div class="container-fluid">
          <!-- Back Button Container -->
          <div class="row mx-auto pb-3  back-button-cont">
            <div class="col-12 back-button">
              <div class="container px-0 mx-auto row">
                <div class="col-12 px-0">
                  <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                      <svg class="arrow-icon" width="32" height="32">
                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                          <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                          <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                        </g>
                      </svg>
                    </span>
                    Back to what we do
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="education-platforms" class="container-fluid mx-auto vertical-container">
        <div class="container-fluid">
          <!-- Back Button Container -->
          <div class="row mx-auto pb-3 back-button-cont">
            <div class="col-12 back-button">
              <div class="container px-0 mx-auto row">
                <div class="col-12 px-0">
                  <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                      <svg class="arrow-icon" width="32" height="32">
                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                          <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                          <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                        </g>
                      </svg>
                    </span>
                    Back to what we do
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- Back Button Container End-->
          <!-- Vertical Container -->
          <div class="row mx-auto vertical-cont-inner">
            <div class="col-12">
              <div class="container">
                <div class="row">
                  <!-- Verticals Title-->
                  <div class="row title">
                    <div class="col-12">
                      <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                        Education Platforms</h2>
                    </div>
                  </div>
                  <!-- Verticals Companies-->
                  <div class="row companies">
                    <div class="col-12 col-md-8 pb-5 mx-auto text-center hero-text">
                      <p class="mx-auto text-center axios-text-light">Products and services are just a part of the
                        FinTech puzzle and they don’t mean much without the complementary education. In order to attract
                        and retain customers to our brands, we understand that providing them with the necessary
                        knowledge about the products, services and industry is of the utmost importance. Education is a
                        simple yet effective way of giving back, empowering people to make informed decisions but also
                        contribute to the overall betterment of the industry through their feedback and experiences.</p>
                    </div>
                    <div class="col-12 col-md-8 pb-5 company-cont mx-auto">
                      <div class="position-relative d-flex m-auto company-img">
                        <div class="mx-auto bg-img mb-3"><img alt="traders-education" class="img-fluid"
                            src="/wp-content/themes/axios-wp-theme/assets/images/verticals/traders-education.png"></div>
                      </div>
                      <div class="text-center text-cont">
                        <span class="d-block vertical-title">Traders Education</span>
                        <p class="text-center">Traders Education is poised to disrupt the market. The platform aims at offering a customized educational solution for brokerages all over the world. The company's signature “Knowledge Base” platform contains educational tools, ebooks and 15 online courses with 300 lessons in 30+ topics taught by financial professionals and translated into over 30 languages. For the past 6 years, the company has been providing unique technology-driven educational solutions catered to brokers that want to use customer training as a powerful vehicle to increase return on their marketing activities and stand out from the competition.</p>
                        <a class="btn-axios btn-axios-light" href="https://www.traders-education.com/" target="_blank">visit
                          website</a>
                      </div>
                    </div>
                  </div>
                  <!-- Verticals Companies End-->
                </div>
              </div>
            </div>
          </div>
          <!-- Vertical Container End-->
        </div>
      </div>
      <div id="technology-providers" class="container-fluid mx-auto vertical-container">
        <div class="container-fluid">
          <!-- Back Button Container -->
          <div class="row mx-auto pb-3 back-button-cont">
            <div class="col-12 back-button">
              <div class="container px-0 mx-auto row">
                <div class="col-12 px-0">
                  <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                      <svg class="arrow-icon" width="32" height="32">
                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                          <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                          <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                        </g>
                      </svg>
                    </span>
                    Back to what we do
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- Back Button Container End-->
          <!-- Vertical Container -->
          <div class="row mx-auto vertical-cont-inner">
            <div class="col-12">
              <div class="container">
                <div class="row">
                  <!-- Verticals Title-->
                  <div class="row title">
                    <div class="col-12">
                      <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                        Technology Providers</h2>
                    </div>
                  </div>
                  <!-- Verticals Companies-->
                  <div class="row companies">
                    <div class="col-12 col-md-8 pb-5 mx-auto text-center hero-text">
                      <p class=" mx-auto text-center axios-text-light">The future of the DevOps vertical is quite clear:
                        software should be intuitive to use on any device in spite of location and geography.
                        Understanding that helps us build software that’s using microservices and automations to ensure
                        system stability. Working with cloud hosting providers, we provide easily-scalable platforms
                        which can grow at the same speed as your business. The challenge here is to not recycle and
                        repackage products and services that already exist but try to add value to business through
                        innovation, without any delays or downtimes.</p>
                    </div>
                    <div class="col-12 col-md-8 pb-5 company-cont mx-auto">
                      <div class="position-relative d-flex m-auto company-img">
                        <div class="mx-auto bg-img mb-4"><img alt="new-age-logo" class="img-fluid"
                            src="/wp-content/themes/axios-wp-theme/assets/images/verticals/lavateck.png"></div>
                      </div>
                      <div class="text-center text-cont">
                        <span class="d-block vertical-title">LavaTeck</span>
                        <p class="text-center">LavaTeck is a technology hub for the companies under the umbrella of Axios Holding. It helps incubate and build startups of the holding by providing them with proprietary tech solutions. In particular, the company develops software for Forex platforms, Forex CRM, and online user engagement platforms. Being a part of such a diverse fintech corporation as Axios, LavaTeck has a broader view compared to other technology providers, which allows it to find creative solutions to the technical problems and develop products of the highest quality. The company’s vision is to be the tech incubator of Axios Holding and support it throughout its rapid growth.</p>
                        <a class="btn-axios btn-axios-light" href="https://lavateck.com/" target="_blank">visit website</a>
                      </div>
                    </div>
                  </div>
                  <!-- Verticals Companies End-->
                </div>
              </div>
            </div>
          </div>
          <!-- Vertical Container End-->
        </div>
      </div>
      <div id="physical-payments" class="container-fluid mx-auto vertical-container">
        <div class="container-fluid">
          <!-- Back Button Container -->
          <div class="row mx-auto pb-3 back-button-cont">
            <div class="col-12 back-button">
              <div class="container px-0 mx-auto row">
                <div class="col-12 px-0">
                  <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                      <svg class="arrow-icon" width="32" height="32">
                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                          <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                          <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                        </g>
                      </svg>
                    </span>
                    Back to what we do
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- Back Button Container End-->
          <!-- Vertical Container -->
          <div class="row mx-auto vertical-cont-inner">
            <div class="col-12">
              <div class="container">
                <div class="row">
                  <!-- Verticals Title-->
                  <div class="row title">
                    <div class="col-12">
                      <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                        Payment Aggregator</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Vertical Container End-->
        </div>
      </div>
      <div id="trading-platforms" class="container-fluid mx-auto vertical-container">
        <div class="container-fluid">
          <!-- Back Button Container -->
          <div class="row mx-auto pb-3 back-button-cont">
            <div class="col-12 back-button">
              <div class="container px-0 mx-auto row">
                <div class="col-12 px-0">
                  <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                      <svg class="arrow-icon" width="32" height="32">
                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                          <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                          <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                        </g>
                      </svg>
                    </span>
                    Back to what we do
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- Back Button Container End-->
          <!-- Vertical Container -->
          <div class="row mx-auto vertical-cont-inner">
            <div class="col-12">
              <div class="container">
                <div class="row">
                  <!-- Verticals Title-->
                  <div class="row title">
                    <div class="col-12">
                      <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                        Investment Platforms</h2>
                    </div>
                  </div>
                  <!-- Verticals Companies-->
                  <div class="row companies">

                    <div class="col-12 col-md-8 pb-5 mx-auto text-center hero-text">
                      <p class=" mx-auto text-center axios-text-light">Investment platforms are no longer an off the
                        shelf product. The dynamic environment and countless variables of the investment world, call for
                        technology that’s adjustable and malleable enough to serve different use cases. The challenge of
                        creating and building our own technology is both inspiring and rewarding. Our goal is to put
                        together platforms that not only serve the end user, but push the boundaries of what was thought
                        to be possible.</p>
                    </div>

                    <div class="col-12 col-md-8 pb-5 company-cont mx-auto">
                      <div class="position-relative d-flex m-auto company-img">
                        <div class="mx-auto bg-img mb-4"><img alt="global-innovation-fund" class="img-fluid"
                            src="/wp-content/themes/axios-wp-theme/assets/images/verticals/global-innovation-fund.jpg">
                        </div>
                      </div>
                      <div class="text-center text-cont">
                        <span class="d-block vertical-title">Global Innovations Fund</span>
                        <p class="text-center">The Global Innovations Fund, established in 2019, provides investors with
                          exposure to the fast-growing verticals in Europe. The purpose of the GI Fund is to provide
                          investors with access to unique opportunities in various sectors of the economy. Its primary
                          focus is in projects relating to Agri & Environment, Energy & Transport, Technology and Real
                          Estate. The GI Fund is an innovative investment vehicle which seeks to pair project owners
                          with international investors; through a rigorous project selection process coupled with a
                          personal approach. The GI Fund team brings together decades of experience at high level
                          investment management with a strong track record in high returns and successful exits.</p>
                        <a class="btn-axios btn-axios-light" href="https://gifund.eu" target="_blank">visit website</a>
                      </div>
                    </div>
                  </div>
                  <!-- Verticals Companies End-->
                </div>
              </div>
            </div>
          </div>
          <!-- Vertical Container End-->
        </div>
      </div>
      <div id="online-payments" class="container-fluid mx-auto vertical-container">
        <div class="container-fluid">
          <!-- Back Button Container -->
          <div class="row mx-auto pb-3 back-button-cont">
            <div class="col-12 back-button">
              <div class="container px-0 mx-auto row">
                <div class="col-12 px-0">
                  <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                      <svg class="arrow-icon" width="32" height="32">
                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                          <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                          <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                        </g>
                      </svg>
                    </span>
                    Back to what we do
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- Back Button Container End-->
          <!-- Vertical Container -->
          <div class="row mx-auto vertical-cont-inner">
            <div class="col-12">
              <div class="container">
                <div class="row">
                  <!-- Verticals Title-->
                  <div class="row title">
                    <div class="col-12">
                      <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                        Online Payments</h2>
                    </div>
                  </div>
                  <!-- Verticals Companies-->
                  <div class="row companies">
                    <div class="col-12 col-md-8 pb-5 mx-auto text-center hero-text">
                      <p class=" mx-auto text-center axios-text-light">Online payments have overshadowed physical
                        payments for quite some time now and the reasons why that’s happening are simple: it’s easier,
                        faster and more convenient. Online payments have quickly evolved from a luxury feature to a
                        necessity, an integral part of any business that wants to be taken seriously. The obvious
                        challenge of this vertical was and will always be the security of funds and protection of
                        sensitive information. Our state-of-the-art technology and custom-built platforms are paving the
                        way for a new era of online payments, catering to the needs of modern businesses whilst never
                        failing to provide the highest form of security.</p>
                    </div>
                    <div class="col-12 col-md-8 pb-5 company-cont mx-auto">
                      <div class="position-relative d-flex m-auto company-img">
                        <div class="mx-auto bg-img mb-4"><img alt="big-wallet-logo" class="img-fluid"
                            src="/wp-content/themes/axios-wp-theme/assets/images/verticals/big-wallet.png"></div>
                      </div>
                      <div class="text-center text-cont">
                        <span class="d-block vertical-title">BigWallet Payments</span>
                        <p class="text-center">BigWallet Payments, a brand of Silvergate Technologies Limited, is a full-fledged
                          Online Payment Solution Provider. It holds a European Payment Institution license from the
                          Central Bank of Cyprus. BigWallet Payments’s vision is to disrupt the traditional banking sector and
                          give people and companies freedom to receive, use and transfer money whenever and wherever
                          they want, at a click of a button. </p>
                        <a class="btn-axios btn-axios-light" href="https://bigwpay.com" target="_blank">visit
                          website</a>
                      </div>
                    </div>
                  </div>
                  <!-- Verticals Companies End-->
                </div>
              </div>
            </div>
          </div>
          <!-- Vertical Container End-->
        </div>
      </div>
      <div id="online-lending" class="vertical-container">
        <div class="container-fluid">
          <!-- Back Button Container -->
          <div class="row mx-auto pb-3 back-button-cont">
            <div class="col-12 back-button">
              <div class="container px-0 mx-auto row">
                <div class="col-12 px-0">
                  <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                      <svg class="arrow-icon" width="32" height="32">
                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                          <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                          <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                        </g>
                      </svg>
                    </span>
                    Back to what we do
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- Back Button Container End-->
          <!-- Vertical Container -->
          <div class="row mx-auto vertical-cont-inner">
            <div class="col-12">
              <div class="container">
                <div class="row">
                  <!-- Verticals Title-->
                  <div class="row title">
                    <div class="col-12">
                      <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                        Online Lending</h2>
                    </div>
                  </div>
                  <!-- Verticals Companies-->
                  <div class="row companies">

                    <div class="col-12 col-md-8 pb-5 mx-auto text-center hero-text">
                      <p class=" mx-auto text-center axios-text-light">Online lending is a relatively new vertical as
                        brick and mortar banking has dominated the space in the past. The growth of the Internet and
                        development of the peer-to-peer lending framework allowed licensed online lending to flourish
                        and push the boundaries of personal finance options. Whilst new, the industry has come a long
                        way since its inception allowing people to access money with a few clicks. Our involvement with
                        this vertical has a very clear mission: provide fast, easy access to money to everyone without
                        any hassle or bureaucracy. </p>
                    </div>

                    <div class="col-12 col-md-8 pb-5 company-cont mx-auto">
                      <div class="position-relative d-flex m-auto company-img">
                        <div class="mx-auto bg-img mb-4"><img alt="equfin-logo" class="img-fluid"
                            src="/wp-content/themes/axios-wp-theme/assets/images/verticals/equfin.jpg"></div>
                      </div>
                      <div class="text-center text-cont">
                        <span class="d-block vertical-title">Equfin</span>
                        <p class="text-center">Equfin Holding was established in 2012 with the vision of giving anyone
                          with an Internet connection access to finance through the latest technologies and most
                          advanced channels. The company has been growing fast and steadily ever since, with presence
                          and significant market share in Spain, Georgia and Ukraine, and Armenia. Equfin’s product
                          range includes short-term consumer loans to people with financial deficit. Its customer-driven
                          service and an advanced in-house online lending platform make it a prominent name in the
                          online lending space. The company currently employs over 250 people across its five
                          operational locations. </p>
                        <a class="btn-axios btn-axios-light" href="https://equfin.com/" target="_blank">visit
                          website</a>
                      </div>
                    </div>
                  </div>
                  <!-- Verticals Companies End-->
                </div>
              </div>
            </div>
          </div>
          <!-- Vertical Container End-->
        </div>
      </div>
      <!-- Exclusive Partners-->
      <div id="exclusive-partners" class="vertical-container">
        <div class="container-fluid">
          <!-- Back Button Container -->
          <div class="row mx-auto pb-3 back-button-cont">
            <div class="col-12 back-button">
              <div class="container px-0 mx-auto row">
                <div class="col-12 px-0">
                  <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                      <svg class="arrow-icon" width="32" height="32">
                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                          <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                          <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                        </g>
                      </svg>
                    </span>
                    Back to what we do
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- Back Button Container End-->
          <!-- Vertical Container -->
          <div class="row mx-auto vertical-cont-inner">
            <div class="col-12">
              <div class="container">
                <div class="row">
                  <!-- Verticals Title-->
                  <div class="row title">
                    <div class="col-12">
                      <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                        Exclusive Partners</h2>
                    </div>
                  </div>
                  <!-- Verticals Companies-->
                  <div class="row companies">
                    <div class="col-12 col-md-8 pb-5 mx-auto text-center hero-text">
                      <p class=" mx-auto text-center axios-text-light"></p>
                    </div>
                    <div class="col-12 col-md-12 pb-5 company-cont">
                      <div class="position-relative d-flex m-auto company-img">
                        <div class="mx-auto bg-img mb-4"><img alt="fincue-logo" class="img-fluid"
                            src="/wp-content/themes/axios-wp-theme/assets/images/verticals/fincue.jpg"></div>
                      </div>

                      <div class="text-center text-cont">
                        <span class="d-block vertical-title">Fincue</span>
                        <p class="text-center">Fincue’s cloud based loan management CRM platform is a true leader in the
                          niche. The technology supports the whole business cycle, with full automation on the majority
                          of loan application and disbursement processes and robust integration with third party service
                          providers. The company offers both a sound technological backbone for online lending providers
                          and granular insights in data analysis. A best of breed and speed solution. </p>
                        <a class="btn-axios btn-axios-light" href="http://www.fincue.com/" target="_blank">visit
                          website</a>
                      </div>
                    </div>
                  </div>
                  <!-- Verticals Companies End-->
                </div>
              </div>
            </div>
          </div>
          <!-- Vertical Container End-->
        </div>
      </div>
      <!-- KYC & ID Verification -->
      <div id="kyc" class="vertical-container">
        <div class="container-fluid">
          <!-- Back Button Container -->
          <div class="row mx-auto pb-3 back-button-cont">
            <div class="col-12 back-button">
              <div class="container px-0 mx-auto row">
                <div class="col-12 px-0">
                  <a href="#" class="mx-auto mx-md-0 pt-4 pt-md-0 text-uppercase">
                    <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
                      <svg class="arrow-icon" width="32" height="32">
                        <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                          <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                          <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                        </g>
                      </svg>
                    </span>
                    Back to what we do
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- Back Button Container End-->
          <!-- Vertical Container -->
          <div class="row mx-auto vertical-cont-inner">
            <div class="col-12">
              <div class="container">
                <div class="row">
                  <!-- Verticals Title-->
                  <div class="row title">
                    <div class="col-12">
                      <h2 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                        KYC and Identity Verification</h2>
                    </div>
                  </div>
                  <!-- Verticals Companies-->
                  <div class="row companies">
                    <div class="col-12 col-md-8 pb-5 mx-auto text-center hero-text">
                      <p class=" mx-auto text-center axios-text-light"></p>
                    </div>
                    <div class="col-12 col-md-12 pb-5 company-cont">
                      <div class="text-center text-cont">
                        <p class="text-center">KYC (Know Your Customer) and Identity Verification are quickly becoming an
                          absolute must-have for many businesses. Besides being a legal requirement in a growing number
                          of industries, it’s a necessity in the era of digital transformation, large-scale data
                          breaches, identity theft, and account takeovers. Businesses need a reliable way to determine
                          if a user is indeed who they claim to be online.</p>
                      </div>

                      <div class="position-relative d-flex m-auto company-img">
                        <div class="mx-auto"><iframe width="560" height="315"
                            src="https://www.youtube.com/embed/jw5ZYSOiXs8" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe></div>
                      </div>

                      <div class="text-center text-cont">
                        <a href="https://identomat.com/"><span class="d-block vertical-title">Identomat</span></a>
                        <p class="text-center">Identomat was created with the belief that it’s possible to turn the KYC
                          process into a truly rewarding experience for the customer. The flagship product was put
                          together with the mission of enabling businesses to onboard customers faster and easier while
                          maintaining the highest level of security. The secret to success is a minimal intuitive design
                          and frictionless automation that can empower the user to seamlessly go through the process.
                        </p>
                        <p class="text-center">Identomat brings businesses the benefits of:</p>
                        <ul class="text-center">
                          <li>Simplifying the user experience without compromising security</li>
                          <li>Eliminating manual identity verification processes</li>
                          <li>Accelerating customer onboarding and transactions</li>
                          <li>Catching fake IDs and prevent fraud</li>
                          <li>Industry-leading data extraction accuracy</li>
                          <li>Omnichannel verification</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- Verticals Companies End-->
                </div>
              </div>
            </div>
          </div>
          <!-- Vertical Container End-->
        </div>
      </div>
      <!-- Verticals End-->
      <div
        class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none">
      </div>
    </div>
  </main>

  <?php include("components/_footer.php"); ?>
  <?php include("components/_scripts.php"); ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/nextparticle.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/attract.min.js"></script>
  <script>
    $(window).on('load', function () {
      function is_touch_device() {
        return (('ontouchstart' in window) ||
          (navigator.MaxTouchPoints > 0) ||
          (navigator.msMaxTouchPoints > 0));
      }

      if (!is_touch_device()) {
        $(".vertical-cont-inner").niceScroll({
          cursorwidth: 5,
          cursorborder: 0,
          cursorcolor: '#0A0B0B',
          autohidemode: true,
          zindex: 999999999,
          horizrailenabled: false,
          cursorborderradius: 0,
        });
      }

      $('#hotspot-container a').on('click', function (e) {
        e.preventDefault();
        var vertical_id = $(this).data('slide');
        $('#what-we-do-slides').addClass('active');
        $('#' + vertical_id).addClass('active');
        var tweenPosts = new TimelineMax();
        tweenPosts.add([
          TweenMax.staggerFromTo(".vertical-container.active .company-cont", 0.4, {
            x: "-220px",
            opacity: '0'
          }, {
            ease: Power1.easeOut,
            x: 0,
            opacity: '1',
            delay: 0.8
          }, 0.15),
        ]);
      });
      $('.back-button').on('click', function (e) {
        e.preventDefault();
        $('#what-we-do-slides').removeClass('active');
        $('.vertical-container').each(function () {
          $(this).removeClass('active');
        })
      });
    });
  </script>
</body>

</html>
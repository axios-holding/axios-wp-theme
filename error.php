<?php
/**
 * Template Name: Error
 */?>
<!doctype html>
<html lang="en" style="height: 100%;">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!--JQuery-->
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery-3.3.1.min.js"></script>
  <!--Bootstrap 4.2.1-->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.css" rel="stylesheet">

  <title>Axios Holding</title>
  <style>
    body {
      font-family: "Montserrat", sans-serif;
    }

    .flashlight {
      position: absolute;
      z-index: -1;
      height: 150px;
      width: 150px;
      border-radius: 150px;
      background-color: rgba(255, 255, 255, 0.9);
      box-shadow: 1px 1px 100px #fff;
    }

    img {
      width: 65%;
    }
  </style>
  <script>
    $(document).ready(function () {
      /*====HIDDING THE FLASHLIGHT ====*/
      $('.flashlight').hide();
      $('body').css("background-color", "black");
      if ($(window).width() < 960) {
        document.body.style.cursor = 'default';
        $('.flashlight').css('display', 'none');
      } else {
        $('.flashlight').fadeIn('5000');
        document.body.style.cursor = 'none';

        $("body").mousemove(function (e) {
          $('.flashlight').css('top', e.clientY - 150).css('left', e.clientX - 150);
        });
      }
      $("body").click(function () {
        window.location.href = 'home';
      });
    });
  </script>
  <?php include("components/_metatags.php"); ?>
</head>

<body style="height: 100%;">

  <main style="width:100%; height: 100%;">
    <div class="container py-5">
      <div class="flashlight"></div>
      <div class="row">
        <div class="col-12 mx-auto text-center">
          <h1 class="text-black text-center display-3 py-4">Lights out. <small>Only temporarily.</small></h1>
        </div>
      </div>
      <div class="row">
        <div class="col-12 mx-auto text-center">
          <h3 class="text-black text-center py-4 text-secondary">Head back to the homepage till we find the switch.</h3>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-12 mx-auto text-center py-5">
          <img src="/wp-content/themes/axios-wp-theme/assets/images/axios_not_found.png" alt="axios-not-found">
        </div>
      </div>
    </div>
    </div>
  </main>
</body>

</html>
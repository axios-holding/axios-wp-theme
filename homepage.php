<?php
/**
 * Template Name: main page
 */?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description"
    content="Axios Holding | A group of Financial Technology companies, an innovation hub, a breeding ground of ideas, plus an end-to-end FinTech incubator with innovation.">
  <?php include("components/_styles.php"); ?>
  <title>Axios Holding</title>
  <?php include("components/_metatags.php"); ?>
  <style>
    .home-white {
      color: #eee!important;  
    }
</style>
</head>

<body>

  <?php include("components/_header.php"); ?>

  <main class="home">
    <!--Hero Section-->
    <div class="home-hero position-relative">
      <div class="home-hero-video">

      </div>
      <div class="position-absolute d-flex justify-content-center w-100 home-hero-header">
        <h1 class="text-white text-center"><?php echo get_field('title') ?><span class="d-block thin"
            style="font-weight: 400!important;"><?php echo get_field('subtitle') ?></span></h1>
        <div id="indicator" class="animate-indicator">
          <div id="scroll-indicator-animation">
            <span class="d-block scroll-indicator"></span>
          </div>
        </div>
      </div>
      <div class="home-block-separator angled-separator flip-x hero-separator"></div>
    </div>

    <!--Section 1-->
    <div id="home-section-1" class="py-5 home-section-1 axios-bg-light">
      <div class="container py-4 animate-fade">
        <div class="row">
          <div class="col-12 col-sm-10 col-lg-8 mx-auto text-center">
            <?php if( have_rows('ecosystem') ): //parent group field
                while( have_rows('ecosystem') ): the_row(); 
                // vars
                $title = get_sub_field('title');
                $text = get_sub_field('text');
            ?>
            <h2 class="axios-text-dark underline"><?php echo $title ?></h2>
            <p><?php echo $text ?></p>
            <a class="btn-axios btn-axios-dark" href="<?php echo esc_url(home_url() . '/what-we-do/');?>">view all
              verticals</a>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

    <!--Section 2-->
    <div id="home-section-2" class="home-section-2 position-relative axios-bg-dark">
      <div
        class="home-block-separator separator-top position-absolute fixed-top angled-separator invert separator-bg-none">
      </div>
      <div class="home-section-2-container container animate-fade">
        <div class="row">
          <div class="col-12 col-sm-10 col-lg-6 mx-auto text-center">
            <?php if( have_rows('investors') ): //parent group field
                while( have_rows('investors') ): the_row(); 
                // vars
                $title = get_sub_field('title');
                $text = get_sub_field('text');
            ?>
            <h2 class="axios-text-light-white underline underline-light"><?php echo $title ?></h2>
            <p class="axios-text-light"><?php echo $text ?></p>
            <a class="btn-axios btn-axios-light" href="<?php echo esc_url(home_url() . '/investors-overview/');?>">LEARN
              MORE</a>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div
        class="home-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x-y separator-bg-none">
      </div>
    </div>

    <!--Section 3-->
    <div id="home-section-3" class="py-5 home-section-3 axios-bg-light">
      <div class="container py-4 animate-fade">
        <div class="row">
          <div class="col-12 col-sm-10 col-lg-6 mx-auto text-center">
            <?php if( have_rows('careers') ): //parent group field
                while( have_rows('careers') ): the_row(); 
                // vars
                $title = get_sub_field('title');
                $text = get_sub_field('text');
            ?>
            <h2 class="axios-text-dark underline"><?php echo $title ?></h2>
            <p><?php echo $text ?></p>
            <a class="btn-axios btn-axios-dark" href="<?php echo esc_url(home_url() . '/careers/'); ?>">new
              vacancies</a>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

    <!--Section 4-->
    <div id="home-section-4" class="home-section-4 position-relative axios-bg-dark">
      <div
        class="home-block-separator separator-top position-absolute fixed-top angled-separator invert flip-y separator-bg-none">
      </div>
      <div class="home-section-4-container py-2 py-sm-5 animate-fade">
       <?php include("components/_blog-section.php"); ?>
      </div>
      <div
        class="home-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none">
      </div>
    </div>
  </main>

  <?php include("components/_footer.php"); ?>
  <?php include("components/_scripts.php"); ?>
  <!--Count To Number Javascript File-->
  <script>
    $(window).on('load resize orientationchange', function () {
      if ($(window).width() > 767) {
        $video =
          '<video autoplay muted loop poster="/wp-content/themes/axios-wp-theme/assets/images/headers/hero_video_poster.jpg" id="bgvid"><source src="/wp-content/themes/axios-wp-theme/assets/images/headers/heroVideo.mp4" type="video/mp4"></video>';
        $('.home-hero-video').html($video);
      } else {
        $img = '<div class="bg-img video-bg-img"></div>';
        $('.home-hero-video').html($img);
      }
    });
    $(window).on('load ', function () {

      var homepage_scroll_ctrl = new ScrollMagic.Controller();


      /***************
       * Home Section 1 Scroll Reveal Animation
       **************/
      var tween_homepage_section_1 = new TimelineMax();
      tween_homepage_section_1.add([
        TweenMax.fromTo("#home-section-1 .animate-fade h2", 1.5, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
        TweenMax.fromTo("#home-section-1 .animate-fade p", 1.3, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
        TweenMax.fromTo("#home-section-1 .animate-fade .btn-axios", 1.1, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
      ]);
      var scene_homepage_homepage_section_1 = new ScrollMagic.Scene({
        triggerElement: '#home-section-1',
        triggerHook: 'onEnter',
        offset: 100,
      });
      scene_homepage_homepage_section_1.setTween(tween_homepage_section_1);
      scene_homepage_homepage_section_1.addTo(homepage_scroll_ctrl);
      scene_homepage_homepage_section_1.reverse(true);

      /***************
       * Home Section 2 Scroll Reveal Animation
       **************/
      var tween_homepage_section_2 = new TimelineMax();
      tween_homepage_section_2.add([
        TweenMax.fromTo("#home-section-2 .animate-fade h2", 1.5, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
        TweenMax.fromTo("#home-section-2 .animate-fade p", 1.3, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
        TweenMax.fromTo("#home-section-2 .animate-fade .btn-axios", 1.1, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
      ]);
      var scene_homepage_homepage_section_2 = new ScrollMagic.Scene({
        triggerElement: '#home-section-2',
        triggerHook: 'onEnter',
        offset: 100,
      });
      scene_homepage_homepage_section_2.setTween(tween_homepage_section_2);
      scene_homepage_homepage_section_2.addTo(homepage_scroll_ctrl);
      scene_homepage_homepage_section_2.reverse(true);

      /***************
       * Home Section 3 Scroll Reveal Animation
       **************/
      var tween_homepage_section_3 = new TimelineMax();
      tween_homepage_section_3.add([
        TweenMax.fromTo("#home-section-3 .animate-fade h2", 1.5, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
        TweenMax.fromTo("#home-section-3 .animate-fade p", 1.3, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
        TweenMax.fromTo("#home-section-3 .animate-fade .btn-axios", 1.1, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
      ]);
      var scene_homepage_homepage_section_3 = new ScrollMagic.Scene({
        triggerElement: '#home-section-3',
        triggerHook: 'onEnter',
        offset: 100,
      });
      scene_homepage_homepage_section_3.setTween(tween_homepage_section_3);
      scene_homepage_homepage_section_3.addTo(homepage_scroll_ctrl);
      scene_homepage_homepage_section_3.reverse(true);

      /***************
       * Home Section 4 Scroll Reveal Animation
       **************/
      var tween_homepage_section_4 = new TimelineMax();
      tween_homepage_section_4.add([
        TweenMax.fromTo("#home-section-4 .animate-fade h2", 1.5, {
          opacity: '0'
        }, {
          ease: Power2.easeOut,
          opacity: '1'
        }),
        TweenMax.staggerFromTo("#home-section-4 .animate-fade .article-cont", 0.4, {
          x: "-220px",
          opacity: '0'
        }, {
          ease: Power1.easeOut,
          x: 0,
          opacity: '1'
        }, 0.15),
      ]);
      var scene_homepage_homepage_section_4 = new ScrollMagic.Scene({
        triggerElement: '#home-section-4',
        triggerHook: 'onEnter',
        offset: 100,
      });
      scene_homepage_homepage_section_4.setTween(tween_homepage_section_4);
      scene_homepage_homepage_section_4.addTo(homepage_scroll_ctrl);
      scene_homepage_homepage_section_4.reverse(true);


    });
  </script>
</body>

</html>
<?php
/**
 * Template Name: Blog Article
 */?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <?php include("components/_styles.php"); ?>

  <title><?php the_title(); ?></title>

  <?php include("components/_metatags.php"); ?>
  <?php wp_head(); ?>
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Article",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "<?php the_permalink()?>"
      },
      "headline": "<?php the_title(); ?>",
      "image": [
        "<?php the_post_thumbnail_url(); ?>"
      ],
      "datePublished": "<?php echo get_the_date('d/m/Y'); ?>",
      "dateModified": "<?php the_modified_date('d/m/Y'); ?>",
      "author": {
        "@type": "Person",
        "name": "Axios"
      },
      "publisher": {
        "@type": "Organization",
        "name": "Axios",
        "logo": {
          "@type": "ImageObject",
          "url": "https://axiosholding.com/wp-content/themes/axios/assets/img/axios-logo_horizontal.png"
        }
      },
      "description": "<?php echo get_the_excerpt(); ?>"
    }
  </script>
  <style>
    .article-text {
      width: 350px !important;
    }

    a.at-icon-wrapper.at-share-btn.at-svc-compact {
      display: none;
    }

    #blog-article ol li a {
      display: initial !important;
    }

    figure.wp-block-image img {

      max-width: 60%;
    }

    .tenor-gif-embed {
      height: 300px;
    }

    .tenor-gif-embed iframe {
      width: 346px !important;
      height: 300px !important;
    }

    #blog-article .main-content .article-content ol {
      padding-bottom: 0;
    }

    .twitter-widget#twitter-widget-0 {
      text-align: center;
      margin: auto;
    }

    .scroll-progress-circle {
      position: fixed;
      top: 150px;
      right: 8px;
      z-index: 10;
      width: 75px;
      height: 75px;
      fill: none;
      stroke: var(--scroll-progress-circle-color, #ffff);
      stroke-width: 3px;
      stroke-dashoffset: 125.6811294555664;
      stroke-dasharray: 125.6811294555664;
      background-color: transparent;
      border-radius: 50%;
      z-index: 10;
    }

    .scroll-progress-circle path {
        fill: url('/wp-content/themes/bigwpay/assets/images/close.svg');
    }

    .scroll-icon {
      position: fixed;
      top: 154px;
      right: 12px;
      width: 67px;
      z-index: 1;
    }
  </style>
</head>

<body>

  <?php include("components/_header.php"); ?>

  <main id="blog-article" class="axios-bg-light blog">
    <a href="<?php echo esc_url(site_url()); ?>/blog/">
        <svg viewBox="0 0 50 50" class="scroll-progress-circle">
            <path d="M25 25 m0 -20 a20 20 0 1 0 0 40 a20 20 0 1 0 0 -40"/>
        </svg>
        <img src="<?php echo get_template_directory_uri() . '/assets/images/favicons/android-chrome-256x256.png'; ?>" class="scroll-icon">
    </a>
    <div class="container-fluid px-0 hero-container">
      <div class="row mx-0">
        <div class="col-12 px-0">
          <div class="hero-content-container"></div>
          <div
            class="hero-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
          </div>
        </div>
      </div>
    </div>
    <div class="main-content py-5 position-relative">
      <div class="container row mx-auto pb-4 back-button-cont">
        <div class="col-6 col-lg-4 px-0 back-button">
          <?php
            if( (has_category( 2 ))) {
          ?>
          <a href="<?php echo esc_url(home_url() . '/press-releases/');?>" class="">
            <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
              <svg class="arrow-icon" width="32" height="32">
                <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                  <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                  <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                </g>
              </svg>
            </span>
            Press Releases
          </a>
          <?php } else { ?>
          <a href="<?php echo esc_url(home_url() . '/blog/'); ?>">
            <span class="d-block pl-3 pl-md-0 arrow-icon-cont">
              <svg class="arrow-icon" width="32" height="32">
                <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                  <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                  <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                </g>
              </svg>
            </span>
            Blog & Media
          </a>
          <?php }?>
        </div>
        <div class="col-6 col-lg-4 px-0 text-right text-lg-center all-button">
          <!--<a href="#"><i class="fas fa-th"></i> All articles</a>-->
        </div>
      </div>
      <div class="blog-article">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md-8 mx-auto">
              <div class="text-center article-title">
                <h1><?php the_title(); ?></h1>
                <h2></h2>
                <span class="d-block article-date"><?php the_date('d/m/Y'); ?></span>
              </div>
              <?php if ( have_posts() ) : ?>
              <?php
                        while ( have_posts() ) : the_post(); ?>
              <div class="article-content">
                <div class="featured-image"> <?php if ( has_post_thumbnail() ) { ?>
                  <img alt="the-post-thumbnail" class="thumb_img" ;" src="<?php the_post_thumbnail_url(); ?>" />
                  <?php } ?></div>
                <?php the_content(); ?>
              </div>
              <div class="blog-article-bottom">
                <div class="text-center tags">Tagged as:
                  <span class="d-flex justify-content-center tags">
                    <?php echo show_tags() ?>
                  </span>
                </div>
                <hr>
                <div class="text-center share-container">
                  <!-- Go to www.addthis.com/dashboard to customize your tools -->
                  <div class="addthis_inline_share_toolbox">

                  </div>

                </div>
              </div>
              <?php endwhile;
                        ?>
              <?php else : ?>

              <?php _e('Sorry, no posts matched your criteria.'); ?>

              <?php endif; ?>

            </div>
          </div>
        </div>
      </div>
      <div id="more-posts" class="py-2 py-sm-5 animate-fade">
        <div class="container articles-container">
          <?php if(has_category( 2 )) {?>
          <div class="row">
            <div class="m-auto text-center">
              <h2 class="axios-text-light-dark underline">More Press Releases</h2>
            </div>
          </div>
          <?php } else{?>
          <div class="row">
            <div class="m-auto text-center">
              <h2 class="axios-text-light-dark underline">More Posts</h2>
            </div>
          </div>
          <?php }?>
          <div class="row pt-3 pb-4">
            <?php
              if( (has_category( 2 ))) {
                  //code to show more press releases

              // the query
              $the_query = new WP_Query( array(
                  'posts_per_page' => 3,
                  'cat' => 2 ,
              ));
            ?>
            <?php if ( $the_query->have_posts() ) : ?>
            <?php $count = 0; ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <?php if ($count == 0) {?>
            <div class="col-12 col-sm-6 col-md-4">
              <div class="mx-auto article-cont">
                <a href="<?php the_permalink(); ?>" class="text-center">
                  <div class="row mx-auto article-img-cont">
                    <div class="position-relative d-flex m-auto article-img-cont-in">
                      <div class="mx-auto bg-img">
                        <?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div>
                    </div>
                  </div>
                  <div class="row mx-auto article-date"><span
                      class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                  <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                </a>
              </div>
            </div>
            <?php  } ?>
            <?php if ($count == 1) {?>
            <div class="d-none d-sm-block col-12 col-sm-6 col-md-4">
              <div class="mx-auto article-cont">
                <a href="<?php the_permalink(); ?>" class="text-center">
                  <div class="row mx-auto article-img-cont">
                    <div class="position-relative d-flex m-auto article-img-cont-in">
                      <div class="mx-auto bg-img">
                        <?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div>
                    </div>
                  </div>
                  <div class="row mx-auto article-date"><span
                      class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                  <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                </a>
              </div>
            </div>
            <?php } ?>
            <?php if ($count == 2) {?>
            <div class="d-none d-md-block col-12 col-sm-6 col-md-4">
              <div class="mx-auto article-cont">
                <a href="<?php the_permalink(); ?>" class="text-center">
                  <div class="row mx-auto article-img-cont">
                    <div class="position-relative d-flex m-auto article-img-cont-in">
                      <div class="mx-auto bg-img">
                        <?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div>
                    </div>
                  </div>
                  <div class="row mx-auto article-date"><span
                      class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                  <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                </a>
              </div>
            </div>
            <?php  } ?>

            <?php $count++ ; endwhile; ?>
            <?php wp_reset_postdata(); ?>

            <?php else : ?>
            <p><?php __('No Postss'); ?></p>
            <?php endif;
                    } else {
                        //code to show more posts

                    // the query
                    $the_query = new WP_Query( array(
                        'posts_per_page' => 3,
                        'category__not_in' => 2 ,
                    ));
                    ?>

            <?php if ( $the_query->have_posts() ) : ?>
            <?php $count = 0; ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <?php if ($count == 0) {?>
            <div class="col-12 col-sm-6 col-md-4">
              <div class="mx-auto article-cont">
                <a href="<?php the_permalink(); ?>" class="text-center">
                  <div class="row mx-auto article-img-cont">
                    <div class="position-relative d-flex m-auto article-img-cont-in">
                      <div class="mx-auto bg-img">
                        <?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div>
                    </div>
                  </div>
                  <div class="row mx-auto article-date"><span
                      class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                  <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                </a>
              </div>
            </div>
            <?php  } ?>
            <?php if ($count == 1) {?>
            <div class="d-none d-sm-block col-12 col-sm-6 col-md-4">
              <div class="mx-auto article-cont">
                <a href="<?php the_permalink(); ?>" class="text-center">
                  <div class="row mx-auto article-img-cont">
                    <div class="position-relative d-flex m-auto article-img-cont-in">
                      <div class="mx-auto bg-img">
                        <?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div>
                    </div>
                  </div>
                  <div class="row mx-auto article-date"><span
                      class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                  <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                </a>
              </div>
            </div>
            <?php } ?>
            <?php if ($count == 2) {?>
            <div class="d-none d-md-block col-12 col-sm-6 col-md-4">
              <div class="mx-auto article-cont">
                <a href="<?php the_permalink(); ?>" class="text-center">
                  <div class="row mx-auto article-img-cont">
                    <div class="position-relative d-flex m-auto article-img-cont-in">
                      <div class="mx-auto bg-img">
                        <?php the_post_thumbnail('full' , array( 'class' => 'img-fluid' ) ); ?></div>
                    </div>
                  </div>
                  <div class="row mx-auto article-date"><span
                      class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span></div>
                  <div class="row mx-auto article-text"><span class="text"><?php the_title(); ?></span></div>
                </a>
              </div>
            </div>
            <?php  } ?>

            <?php $count++ ; endwhile; ?>
            <?php wp_reset_postdata(); ?>

            <?php else : ?>
            <p><?php __('No Postss'); ?></p>
            <?php endif;
                    }
                    ?>
          </div>
          <?php
                    if( (has_category( 2 ))) {
                    ?>
          <div class="row">
            <div class="pt-5 mx-auto text-center all-articles-cont"><a class="btn-axios btn-axios-dark"
                href="<?php echo esc_url(home_url() . '/press-releases/');?>">all press releases</a></div>
          </div>
          <?php } else {?>
          <div class="row">
            <div class="pt-5 mx-auto text-center all-articles-cont"><a class="btn-axios btn-axios-dark"
                href="<?php echo esc_url(home_url() . '/blog/');?>">all news</a></div>
          </div>
          <?php }?>

        </div>
      </div>
      <div
        class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none">
      </div>
    </div>

  </main>

  <?php include("components/_footer.php"); ?>
  <?php include("components/_scripts.php"); ?>
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
  <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c9364cc3658d06e"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <script src="<?php echo get_template_directory_uri() . '/assets/js/dist/scroll-progress.js'; ?>"></script>

  <script>
    $(document).ready(function () {

      var cursor = document.querySelector(".custom-cursor");
      var select = document.querySelectorAll(".at-share-btn");

      for (var i = 0; i < select.length; i++) {
        var selfSelect = select[i];
        console.log(selfSelect);
        selfSelect.addEventListener("mouseover", function () {
          cursor.classList.add("custom-cursor--link");
        });
        selfSelect.addEventListener("mouseout", function () {
          cursor.classList.remove("custom-cursor--link");
        });
      }

    });
    $(window).on('load ', function () {

      var tweenPosts = new TimelineMax()
      tweenPosts.add([
        TweenMax.staggerFromTo("#blog-results .article", 0.4, {
          x: "-220px",
          opacity: '0'
        }, {
          ease: Power1.easeOut,
          x: 0,
          opacity: '1',
          delay: 0.8
        }, 0.15),
      ]);


    });
  </script>
</body>

</html>
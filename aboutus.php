<?php
/**
 * Template Name: About
 */?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Axios Holding - About Us</title>
  <meta name="description"
    content="Our motto is creating the most innovative, comprehensive and embracive FinTech hub. To make it real we leverage our knowledge and technology.">
  <?php include "components/_metatags.php";?>
  <?php include "components/_styles.php";?>
</head>

<body>
  <?php include "components/_header.php";?>
  <main id="about-us">
    <div class="position-relative">
      <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
          <div class="col-12 px-0">
            <div class="bg-img hero-bg">
              <img alt="about-us-header-background"
                src="/wp-content/themes/axios-wp-theme/assets/images/headers/aboutus-header_BG.jpg">
            </div>
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="hero-content-container">
                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                      About us</h1>
                    <div class="content">
                      <h3 class="text-center text-sm-left"><?php echo get_field('title') ?></h3>
                      <div class="text-center text-sm-left hero-text">
                        <p class="axios-text-light"><?php echo get_field('paragraph_1') ?></p>
                        <p class="axios-text-light"><?php echo get_field('paragraph_2') ?></p>
                        <p class="axios-text-light"><?php echo get_field('paragraph_3') ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </main>
  <?php include "components/_footer.php";?>
  <?php include "components/_scripts.php";?>
</body>

</html>
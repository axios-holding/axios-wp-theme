<?php
/**
 * Template Name: Investors overview
 */?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php include("components/_styles.php"); ?>
  <title>Axios Holding - Investors Overview</title>
  <?php include("components/_metatags.php"); ?>
  <meta name="description"
    content="Interested in investment opportunities and relevant information on how you can engage with Axios Holding, get in touch with our dedicated Investor Relations office.">
</head>

<body>
  <?php include("components/_header.php"); ?>
  <main id="investors-overview" class="axios-bg-light">
    <div class="position-relative investors-overview-cont">
      <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
          <div class="col-12 px-0 position-relative hero-inner">
            <div class="bg-img hero-bg">
              <img alt="investors-overview-header"
                src="/wp-content/themes/axios-wp-theme/assets/images/headers/investorsoverview-header_BG.jpg">
            </div>
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="hero-content-container">
                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                      Investors overview</h1>
                    <div class="content mx-auto">
                      <h3 class="pb-3 text-center"><?php echo get_field('title') ?></h3>
                      <p class="text-center axios-text-light"><?php echo get_field('description') ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              class="blog-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
            </div>
          </div>
        </div>
      </div>
      <div id="investors-overview-section" class="py-5">
        <div class="container section-cont">
          <div class="row mx-0 section-cont-inner">
            <div class="row h-100">
              <div class="col-12 col-md-6 py-3 overview-container">
                <?php if( have_rows('ir_contact') ): //parent group field
                while( have_rows('ir_contact') ): the_row(); 
                // vars
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                $linked_page = get_sub_field('linked_page');
              ?>
                <a class="h-100 w-100 axios-bg-white" href="<?php echo $linked_page ?>">
                  <div class="p-4 text-cont">
                    <div class="pl-2 text-cont-in">
                      <span class="d-block pb-2 title"><?php echo $title ?></span>
                      <p class="d-block description"><?php echo $description ?></p>
                      <span class="d-block text-uppercase arrow-icon-cont">Read more<svg class="arrow-icon" width="32"
                          height="32">
                          <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                            <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                            <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                          </g>
                        </svg></span>
                    </div>
                  </div>
                </a>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
              <div class="col-12 col-md-6 py-3 overview-container">
                <?php if( have_rows('press_releases') ): //parent group field
                while( have_rows('press_releases') ): the_row(); 
                // vars
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                $linked_page = get_sub_field('linked_page');
              ?>
                <a class="h-100 w-100 axios-bg-white" href="<?php echo $linked_page ?>">
                  <div class="p-4 text-cont">
                    <div class="pl-2 text-cont-in">
                      <span class="d-block pb-2 title"><?php echo $title ?></span>
                      <p class="d-block description"><?php echo $description ?></p>
                      <span class="d-block text-uppercase arrow-icon-cont">Read more<svg class="arrow-icon" width="32"
                          height="32">
                          <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                            <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                            <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                          </g>
                        </svg></span>
                    </div>
                  </div>
                </a>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none">
      </div>
    </div>
  </main>

  <?php include("components/_footer.php"); ?>
  <?php include("components/_scripts.php"); ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nice-select.min.js"></script>
  <script>
    $(document).ready(function () {

    });
    $(window).on('load ', function () {
      var tweenPosts = new TimelineMax();
      tweenPosts.add([
        TweenMax.staggerFromTo("#investors-overview-section .overview-container", 0.4, {
          x: "-220px",
          opacity: '0'
        }, {
          ease: Power1.easeOut,
          x: 0,
          opacity: '1',
          delay: 1
        }, 0.15),

      ]);
    });
  </script>
</body>

</html>
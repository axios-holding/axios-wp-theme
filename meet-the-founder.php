<?php
/**
 * Template Name: Meet The Founder
 */?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Axios Holding - Meet The Founder</title>
  <meta name="description"
    content="Our motto is creating the most innovative, comprehensive and embracive FinTech hub. To make it real we leverage our knowledge and technology.">
  <?php include "components/_metatags.php";?>
  <?php include "components/_styles.php";?>
</head>

<body>
  <?php include "components/_header.php";?>
  <main id="about-us">
    <div class="position-relative">
      <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
          <div class="col-12 px-0">
            <div class="bg-img hero-bg">
              <img alt="about-us-header-background"
                src="/wp-content/themes/axios-wp-theme/assets/images/headers/about-us-background-new.png">
            </div>
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="hero-content-container">
                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">MEET
                      THE FOUNDER</h1>
                    <div class="content">
                      <h3 class="text-center"><?php echo  get_field('name') ?></h3>
                      <div class="text-center">
                        <img style="width: 150px; height: 150px; margin-bottom: 2rem;"
                        src="<?php echo  get_field('photo') ?>">
                        <p style="max-width:750px; margin: 0 auto; font-size:15px" class="axios-text-light"><?php echo  get_field('description') ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mx-0" style="margin-top: -8rem;">
          <div class="bottom-block-separator separator-bottom fixed-bottom angled-separator separate-black flip-x separator-bg-none"></div>
          <div class="col-12 px-0 axios-bg-black">
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="content">
                    <h3 style="max-width:750px; margin: 0 auto;" class="text-center axios-text-light pt-3">“Axios Holding is where tech ideas come to become a reality. What fuels us is the challenge and thrill of creating technology products that change entire organisations”</h3>
                    <div class="text-center">
                      <p style="max-width:750px; margin: 0 auto; font-size:15px" class="axios-text-light pb-3">Rati Tchelidze, Axios Holding Founder & Director</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="bottom-block-separator separator-bottom fixed-bottom angled-separator separate-black flip-x flip-y separator-bg-none"></div>
        </div>
        <div class="row mx-0 pb-5">
          <div class="col-12 px-0">
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="content">
                    <h3 class="text-center axios-text-dark pt-5">Interview with the Axios Holding Founder<br /> and Director,
                      Rati Tchelidze</h3>
                    <div class="text-center">
                      <p class="text-center axios-text-dark font-weight-bold m-0 pt-5">What’s the inspiration behind Axios
                        Holding?</p>
                      <p style="max-width:750px; margin: 0 auto; font-size:15px" class="axios-text-dark pt-3">Axios Holding
                        is where tech ideas come to become a reality. What fuels us is the challenge and thrill of
                        creating technology products that change entire organisations.</p>

                      <p class="text-center axios-text-dark font-weight-bold m-0 pt-5">Can you describe the Axios culture in a
                        few words?</p>
                      <p style="max-width:750px; margin: 0 auto; font-size:15px" class="axios-text-dark pt-3">There’s no
                        Axios Holding without its people. It’s all about the team-spirit and the togetherness of the
                        talented people we are fortunate to employ. Companies remain ideas without the people that bring
                        them to life and things are no different at Axios.</p>

                      <p class="text-center axios-text-dark font-weight-bold m-0 pt-5">How do you ensure that Axios Holding
                        continues to innovate and develop?</p>
                      <p style="max-width:750px; margin: 0 auto; font-size:15px" class="axios-text-dark pt-3">By staying true
                        to what brought us to the dance in the first place: attention to detail, hard work and never
                        getting complacent. When you work in tech there are no days off. Whenever you take a break or
                        relax, you can be sure that someone else is working hard to achieve the same, if not more than
                        you. What we try to preach and live by at Axios Holding, is to stay hungry and stay motivated no
                        matter how far we have come.</p>

                      <p class="text-center axios-text-dark font-weight-bold m-0 pt-5">What differentiates Axios Holding from
                        other FinTech incubators?</p>
                      <p style="max-width:750px; margin: 0 auto; font-size:15px" class="axios-text-dark pt-3">Investing in
                        the companies and people that enter the family. We understand that people, much like ideas, need
                        time to grow and flourish. They need fuel, attention, patience and nurturing.</p>

                      <p class="text-center axios-text-dark font-weight-bold m-0 pt-5">What’s your vision for Axios Holding?
                      </p>
                      <p style="max-width:750px; margin: 0 auto; font-size:15px" class="axios-text-dark pt-3 pb-5">The vision is
                        quite clear - create a fintech hub where we create technology that aids meaningful change. We
                        want to bring businesses value. We strive to put ourselves in the shoes of the end user on a
                        daily basis in order to create tech that is easy to use.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="bottom-block-separator separator-bottom fixed-bottom angled-separator separate-white flip-x flip-y separator-bg-none"></div>
      </div>
  </main>
  <?php include "components/_footer.php";?>
  <?php include "components/_scripts.php";?>
</body>

</html>
// Scroll Progress.
// ---------------

function scrollProgress (element) {
  const progress = document.querySelector(element)
  const path = progress.querySelector('path')
  const length = path.getTotalLength()

  progress.setAttribute('aria-valuemax', `100%`)
  progress.setAttribute('aria-valuenow', `0%`)
  progress.setAttribute('role', `progress`)
  path.setAttribute('aria-hidden', `true`)

  const scrollable = () => {
    let top = document.body.scrollTop || document.documentElement.scrollTop
    let height = document.documentElement.scrollHeight - document.documentElement.clientHeight
    let scroll = parseFloat(top / height * length)
    let value = parseFloat(top / height * 100).toFixed(0)
    path.setAttribute('stroke-dashoffset', -(length - scroll))
    progress.setAttribute('aria-valuenow', `${value}%`)
  }

  window.addEventListener('scroll', scrollable)
}

scrollProgress('.scroll-progress-circle')
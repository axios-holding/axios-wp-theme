<?php
/**
 * Template Name: Sitemap
 */?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php include("components/_styles.php"); ?>
  <title>Axios Holding</title>
  <?php include("components/_metatags.php"); ?>
</head>

<body>
  <?php include("components/_header.php"); ?>
  <main id="generic-template" class="axios-bg-light sitemap">
    <div class="container-fluid px-0 hero-container">
      <div class="row mx-0">
        <div class="col-12 px-0">
          <div class="hero-content-container"></div>
          <div
            class="hero-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
          </div>
        </div>
      </div>
    </div>
    <div class="position-relative">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="main-content">
              <h1>Sitemap</h1>
              <div class="container m-auto">
                <div class="row">
                  <div class="col-12">
                    <ul class="text-center">
                      <li><a href="/404-2">404</a></li>
                      <li><a href="/about-us">about</a></li>
                      <li><a href="/blog">blog</a></li>
                      <li><a href="/blog-media">blog & media</a></li>
                      <li><a href="/careers">careers</a></li>
                      <li><a href="/contact-us">contact us</a></li>
                      <li><a href="/">home</a></li>
                      <li><a href="/investors-overview">investors overview</a></li>
                      <li><a href="/investors-relations">investors relations</a></li>
                      <li><a href="/media-kit">media kit</a></li>
                      <li><a href="/press-releases">press releases</a></li>
                      <li><a href="/what-we-do">what we do</a></li>
                      <li><a href="/meet-the-founder">meet the founder</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none">
      </div>
    </div>

  </main>

  <?php include("components/_footer.php"); ?>
  <?php include("components/_scripts.php"); ?>
  <script>
    $(document).ready(function () {});
  </script>
</body>

</html>
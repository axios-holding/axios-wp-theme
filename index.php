<?php
/**
 * 
 */?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!--Google Fonts-->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,400,500,600,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,700" rel="stylesheet">
  <!--Font Awesome Icons-->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/fontawesome/css/all.min.css" rel="stylesheet">

  <!--Bootstrap 4.2.1-->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.css" rel="stylesheet">
  <!--Blend Styles-->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css" rel="stylesheet">

  <link rel="apple-touch-icon" sizes="180x180"
    href="/wp-content/themes/axios-wp-theme/assets/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32"
    href="/wp-content/themes/axios-wp-theme/assets/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16"
    href="/wp-content/themes/axios-wp-theme/assets/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="/wp-content/themes/axios-wp-theme/assets/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#0088cf">
  <meta name="apple-mobile-web-app-title" content="Axios Holding Website">
  <meta name="application-name" content="Axios Holding Group Website">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="theme-color" content="#ffffff">

  <style>
    @media screen and (min-width: 992px) {
      * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        cursor: none;
      }

      a {
        cursor: none;
      }

      a:hover {
        color: #eee;
      }

      html {
        height: 100%;
      }

      body {
        min-height: 100%;
        font-family: 'Montserrat', sans-serif;
        overflow-x: hidden;
        background-color: #0a0b0b;
      }

      h1 {
        color: #eee;
      }

      .custom-cursor {
        position: fixed;
        opacity: 0;
        pointer-events: none;
        mix-blend-mode: difference;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        background-color: white;
        transition: transform 350ms ease;
        transform: translate(-50%, -50%) scale(.3);
        z-index: 1000;
      }

      .custom-cursor--link {
        background-color: transparent;
        transform: translate(-50%, -50%) scale(1);
        border: 2px solid white;
      }
    }
  </style>
  <title>Axios Holding</title>
</head>
<body>
  <main class="index">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1>Templates</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <ul class="orderedList">
            <li><a href="<?php echo esc_url(home_url());?>">Homepage</a> </li>
          </ul>
        </div>
      </div>
    </div>
  </main>

  <div class="custom-cursor"></div>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/TweenMax.min.js"></script>

  <script>
    /** Mouse Cursor */
    document.addEventListener("DOMContentLoaded", function (event) {
      var cursor = document.querySelector(".custom-cursor");
      var links = document.querySelectorAll("a");
      var initCursor = false;

      for (var i = 0; i < links.length; i++) {
        var selfLink = links[i];

        selfLink.addEventListener("mouseover", function () {
          cursor.classList.add("custom-cursor--link");
        });
        selfLink.addEventListener("mouseout", function () {
          cursor.classList.remove("custom-cursor--link");
        });
      }

      window.onmousemove = function (e) {
        var mouseX = e.clientX;
        var mouseY = e.clientY;

        if (!initCursor) {
          // cursor.style.opacity = 1;
          TweenLite.to(cursor, 0.3, {
            opacity: 1
          });
          initCursor = true;
        }

        TweenLite.to(cursor, 0, {
          top: mouseY + "px",
          left: mouseX + "px"
        });
      };

      window.onmouseout = function (e) {
        TweenLite.to(cursor, 0.3, {
          opacity: 0
        });
        initCursor = false;
      };
    });
  </script>
</body>

</html>
<?php
/**
 * Template Name: Contact Us
 */?>
<!doctype html>
<html lang="en">

<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <?php include("components/_styles.php"); ?>

  <title>Axios Holding - Contact Us</title>
  <?php include("components/_scripts.php"); ?>
  <?php include("components/_metatags.php"); ?>
  <meta name="description"
    content="Axios Holding | FinTech companies, innovation hub, breeding ground of ideas, end-to-end FinTech incubator.">
</head>

<body>

  <?php include("components/_header.php"); ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery-3.3.1.min.js"></script>
  <script>
    $(function () {
      $(".ajaxForm").submit(function (e) {
        e.preventDefault();
        if ($("#full-name").val().length == 0) {
          $("#name-error").text("Please enter your full name");
          return false;
        } else if ($("#email-address").val().length == 0) {
          $("#name-error").text("");
          $("#email-error").text("Please enter your email address");
          return false;
        } else if ($("#message").val().length == 0) {
          $("#name-error").text("");
          $("#email-error").text("");
          $("#message-error").text("Please enter a message");
          return false;
        } else {
          var href = $(this).attr("action");
          $.ajax({
            type: "POST",
            dataType: "json",
            url: href,
            data: $(this).serialize(),
            success: function (response) {
              if (response.status == "success") {
                window.location.href = "https://axiosholding.com/thank-you/";
              } else {
                alert("An error occured: " + response.message);
              }
            }
          });
        }
      });
    });
  </script>

  <main id="contact-us">
    <div class="position-relative">
      <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
          <div class="col-12 px-0">
            <div class="bg-img hero-bg">
              <img alt="contact-header-bg"
                src="/wp-content/themes/axios-wp-theme/assets/images/headers/contact-header_BG.jpg">
            </div>
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="hero-content-container">
                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                      Contact</h1>
                    <div class="content">
                      <h3 class="text-center">Get in touch with us</h3>
                      <div class="col-12">
                        <div class="form pt-5 pt-lg-0 contact-form">
                          <form name="contactForm" class="ajaxForm" id="contactForm"
                            action="https://formcarry.com/s/VzS6LVM4cLx" method="POST" accept-charset="UTF-8">
                            <span class="d-block form-text text-center form-required"><i class="fas fa-asterisk"></i>
                              Required fields</span>
                            <div class="row mt-4">
                              <div class="col-12 col-md-6">
                                <div class="form-group">
                                  <input type="text" class="form-control" id="full-name" name="full-name">
                                  <label for="full-name">Full Name <i class="fas fa-asterisk"></i></label>
                                  <p style="color: #E4252F; font-size: 13px;" id="name-error"></p>
                                </div>
                              </div>
                              <div class="col-12 col-md-6">
                                <div class="form-group">
                                  <input type="email" class="form-control" id="email-address" name="email-address">
                                  <label for="email-address">Email Address <i class="fas fa-asterisk"></i></label>
                                  <p style="color: #E4252F; font-size: 13px;" id="email-error"></p>
                                </div>
                              </div>
                            </div>
                            <div class="row mt-2">
                              <div class="col-12 col-md-6">
                                <div class="form-group">
                                  <input type="tel" class="form-control" id="phone-number" name="phone-number">
                                  <label for="phone-number">Phone Number </label>
                                </div>
                              </div>
                              <div class="col-12 col-md-6">
                                <div class="form-group">
                                  <input type="text" class="form-control" id="company-organisation"
                                    name="company-organisation">
                                  <label for="company-organisation">Company or Organization </label>
                                </div>
                              </div>
                            </div>
                            <div class="row mt-2">
                              <div class="col-12">
                                <div class="form-group">
                                  <textarea class="form-control" rows="6" id="message" name="message"></textarea>
                                  <label for="message">Your message <i class="fas fa-asterisk"></i></label>
                                  <p style="color: #E4252F; font-size: 13px;" id="message-error"></p>
                                </div>
                              </div>
                            </div>
                            <span class="form-text text-center pb-4 consent">By submitting up, you consent to our <a
                                href="<?php echo esc_url(home_url() . '/privacy/');?>">Privacy Policy</a></span>
                            <div class="row">
                            </div>
                            <div class="row mt-2">
                              <div class="col-sm-12 text-center"><input class="btn-axios btn-axios-light" type="submit"
                                  value="submit"></div>
                            </div>
                            <input type="hidden" name="_gotcha"><!-- use this to prevent spam -->
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              class="about-us-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
            </div>
          </div>
        </div>
      </div>
      <div class="axios-bg-light contact-details-cont">
        <div class="container row mx-auto">
          <div class="col-12 text-center contact-info">
            <h3>HQ Office Details</h3>
            <span class="info-cont address-container">Address<span
                class="info address"><?php echo get_field('address') ?></span></span>
            <span class="info-cont details-container">Details
              <span class="info email"><a
                  href="mailto:<?php echo get_field('email') ?>"><?php echo get_field('email') ?></a></span>
              <span class="info phone"><a
                  href="tel:<?php echo get_field('phone') ?>"><?php echo get_field('phone') ?></a></span>
            </span>
          </div>
          <div class="col-12 pt-4 pt-lg-5 map-container">
            <div class="container">
              <div class="mapouter">
                <div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas"
                    src="https://maps.google.com/maps?q=antheon%202%20monovoliko&t=&z=13&ie=UTF8&iwloc=&output=embed"
                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a
                    href="https://www.pureblack.de"></a></div>
                <style>
                  .mapouter {
                    position: relative;
                    text-align: right;
                    height: 500px;
                    width: 600px;
                  }

                  .gmap_canvas {
                    overflow: hidden;
                    background: none !important;
                    height: 500px;
                    width: 600px;
                  }
                </style>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none">
      </div>
    </div>
  </main>
  <?php include("components/_footer.php"); ?>
</body>

</html>
<?php
/**
 * Template Name: Careers
 */?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php include("components/_styles.php"); ?>
  <title>Axios Holding - Careers</title>
  <?php include("components/_metatags.php"); ?>
  <meta name="description"
    content="Looking for starting a new job entails much more than a job title and a salary. At Axios, we value the commitment and we do all in our power to reciprocate.">
</head>

<body>

  <?php include("components/_header.php"); ?>

  <main id="careers">
    <div class="position-relative">
      <div class="container-fluid px-0 hero-container">
        <div class="row mx-0">
          <div class="col-12 px-0">
            <div class="bg-img hero-bg">
              <img alt="careers-header"
                src="/wp-content/themes/axios-wp-theme/assets/images/headers/careers-header_BG.jpg">
            </div>
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="hero-content-container">
                    <h1 class="axios-text-light-white text-center underline underline-light inner-template-heading">
                      Careers</h1>
                    <?php if( have_rows('header') ): //parent group field
                        while( have_rows('header') ): the_row(); 
                        // vars
                        $title = get_sub_field('title');
                        $text = get_sub_field('text');
                    ?>
                    <h3 class="text-center"><?php echo $title ?></h3>
                    <div class="content">
                      <div class="text-center hero-text">
                        <p class="col-12 col-lg-6 px-0 mx-auto text-center axios-text-light"><?php echo $text ?></p>
                      </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <div class="map-container">
                      <div class="text-center map">
                        <img alt="map-vector-careers" class="img-fluid mx-auto d-md-block"
                          src="/wp-content/themes/axios-wp-theme/assets/images/Map-Vector_Careers.png">
                        <div class="pins-container">
                          <span id="spain" data-select="spain" class="pin"><span class="pin-tooltip">Spain</span></span>
                          <span id="ukraine" data-select="ukraine" class="pin"><span
                              class="pin-tooltip">Ukraine</span></span>
                          <span id="georgia" data-select="georgia" class="pin"><span
                              class="pin-tooltip">Georgia</span></span>
                          <span id="cyprus" data-select="cyprus" class="pin"><span
                              class="pin-tooltip">Cyprus</span></span>
                          <span id="lithuania" data-select="lithuania" class="pin"><span
                              class="pin-tooltip">Lithuania</span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
            </div>
          </div>
        </div>
      </div>
      <div class="careers-positions pt-4 axios-bg-light">
        <div class="container">
          <!-- Careers Container -->
          <div id="BambooHR">
            <script src="https://axiosholding.bamboohr.com/js/jobs2.php" type="text/javascript"></script>
            <div id="BambooHR-Footer">Powered by<a href="http://www.bamboohr.com" target="_blank"
                rel="noopener external nofollow noreferrer"><img
                  src="https://resources.bamboohr.com/images/footer-logo.png" alt="BambooHR - HR software" /></a>
            </div>
          </div>
          <!-- Careers Container End -->

          <!-- Discount Scheme Container-->
          <div class="row discount-container text-center">
            <?php if( have_rows('discount') ): //parent group field
                while( have_rows('discount') ): the_row(); 
                // vars
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $text = get_sub_field('text');
                $image = get_sub_field('image');
            ?>
            <div class="col-12 pb-4">
              <h2 class="axios-text-dark text-center underline underline-light inner-template-heading pt-5">
                <?php echo $title ?></h2>
            </div>
            <div class="col-12 mb-4">
              <div class="bg-img"><img src="<?php echo $image ?>" alt="axios-card-scheme"></div>
            </div>
            <div class="container row mx-auto">
              <div class="col-12 col-lg-5 mx-auto">
                <h3><?php echo $subtitle ?></h3>
              </div>
            </div>
            <div class="col-12 col-lg-8 mx-auto">
              <p><?php echo $text ?></p>
            </div>
            <div class="col-12 text-center"><a class="btn-axios btn-axios-dark"
                href="<?php echo get_template_directory_uri(); ?>/assets/docs/axios-discount-card.pdf"
                target="_blank">See More</a>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <!-- /Discount Scheme Container End-->
        </div>
      </div>
      <div class="container-fluid position-relative hr-message bg-img">
        <?php if( have_rows('hr_message') ): //parent group field
            while( have_rows('hr_message') ): the_row(); 
            // vars
            $title = get_sub_field('title');
            $text = get_sub_field('text');
            $author = get_sub_field('author');
            $image = get_sub_field('background_image');
        ?>
        <img src="<?php echo $image ?>" alt="hr-message">
        <div class="row axios-bg-dark">
          <div class="separator-top position-absolute fixed-top angled-separator invert separator-bg-none">
          </div>
          <div class="col-12 col-sm-10 col-lg-6 mx-auto text-center message-container">
            <h2 class="axios-text-light-white underline underline-light"><?php echo $title ?></h2>
            <p class="axios-text-light text-white"><?php echo $text ?></p>
            <span class="quote-author"><?php echo $author ?></span>
          </div>
          <div class="separator-bottom position-absolute fixed-bottom angled-separator flip-x separator-bg-none">
          </div>
        </div>
      </div>
      <div id="more-posts" class="py-2 pb-sm-5 axios-bg-light">
        <?php include("components/_blog-section.php"); ?>
      </div>
      <div
        class="bottom-block-separator separator-bottom position-absolute fixed-bottom angled-separator invert flip-x separator-bg-none">
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
    </div>

  </main>
  <?php include("components/_footer.php"); ?>
  <?php include("components/_scripts.php"); ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/jquery.nice-select.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/dist/bootstrap-multiselect.min.js"></script>
</body>

</html>
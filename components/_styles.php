<!--Google Fonts-->
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,700,800" rel="stylesheet">
<!--Font Awesome Icons-->
<link href="<?php echo get_template_directory_uri(); ?>/assets/fontawesome/css/all.min.css" rel="stylesheet">

<!--Bootstrap 4.2.1-->
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<!--Equfin Theme Styles-->
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css?v=159<?php echo rand(1000000, 9999999); ?>" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/pagination.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/search-fix.css" rel="stylesheet">
<footer>
  <div class="axios-bg-white">
    <div class="container py-4 px-md-0">
      <div class="row">
        <div class="col-12 text-center logo-container">
          <img alt="axios-logo-vertical" class="logo-img svg"
            src="/wp-content/themes/axios-wp-theme/assets/images/logos/axios-logo_vertical.svg">
        </div>
        <div class="col-12 px-0 d-none d-sm-block footer-menu">
          <ul class="p-0 text-center mb-4">
            <li class="d-inline-block px-3"><a href="<?php echo esc_url(home_url() . '/about-us/'); ?>">about</a></li>
            <li class="d-inline-block px-3"><a href="<?php echo esc_url(home_url() . '/what-we-do/'); ?>">what we do</a>
            <li class="d-inline-block px-3"><a href="<?php echo esc_url(home_url() . '/meet-the-founder/'); ?>">meet the founder</a>
            </li>
            <li class="d-inline-block px-3"><a
                href="<?php echo esc_url(home_url() . '/investors-overview/'); ?>">investors</a></li>
            <li class="d-inline-block px-3"><a href="<?php echo esc_url(home_url() . '/blog-media/'); ?>">blog &
                media</a></li>
            <li class="d-inline-block px-3"><a href="<?php echo esc_url(home_url() . '/careers/'); ?>">careers</a></li>
            <li class="d-inline-block px-3"><a href="<?php echo esc_url(home_url() . '/contact-us/'); ?>">contact</a>
            </li>
          </ul>
        </div>
        <div class="col-12 social-menu">
          <ul class="p-0 d-flex justify-content-center text-center">
            <li class="px-4"><a href="https://www.facebook.com/axiosholding/" target="_blank"><i
                  class="fab fa-facebook-f"></i></a></li>
            <li class="px-4"><a href="https://www.instagram.com/axiosholding/" target="_blank"><i
                  class="fab fa-instagram"></i></a></li>
            <li class="px-4"><a href="https://www.linkedin.com/company/axiosholding" target="_blank"><i
                  class="fab fa-linkedin-in"></i></a></li>
          </ul>
        </div>
        <div class="col-12 text-center position-relative mx-auto py-3 newsletter-form">
          <div class="form mx-auto position-relative text-center">
            <span class="form-text text-left">Subscribe to our monthly Newsletter</span>
            <div class="position-relative pt-2 pb-4 form-field">
              <form id="axios_newsletter" action="https://formcarry.com/s/MMFWT6s7ZMR" method="POST"
                accept-charset="UTF-8">
                <input type="email" name="email" id="newsletter" placeholder="Enter your email address"
                  autocomplete="off" class="w-100">
                <a href="#" class="position-absolute" id="submitNewsletter"
                  onclick="document.getElementById('axios_newsletter').submit();">submit
                  <span class="d-block pl-3 arrow-icon-cont">
                    <svg class="arrow-icon" width="32" height="32">
                      <g fill="none" stroke-width="1.5" stroke-linejoin="round" stroke-miterlimit="10">
                        <circle class="arrow-icon--circle" cx="16" cy="16" r="15.12"></circle>
                        <path class="arrow-icon--arrow" d="M16.14 9.93L22.21 16l-6.07 6.07M8.23 16h13.98"></path>
                      </g>
                    </svg>
                  </span>
                </a>
                <input type="hidden" name="_gotcha"><!-- use this to prevent spam -->
              </form>
            </div>
          </div>
        </div>
        <div class="col-12 copyright-container">
          <div class="d-block text-center mx-auto copyright"><span class="d-block mx-auto mb-3 mb-sm-0 ">© <?php echo date("Y"); ?> Axios
              Holding. All rights reserved.</span></div>
          <div class="d-block mb-3 mb-sm-0 legals-menu">
            <ul class="p-0 text-center mb-1 d-sm-flex justify-content-center">
              <li class="d-block"><a href="<?php echo esc_url(home_url() . '/privacy/'); ?>">Privacy & Cookie Policy</a>
              </li>
            </ul>
          </div>
          <div class="d-block text-center blend"><span class="d-flex justify-content-center blend-link">Website by <a
                class="d-flex px-2" href="http://www.blenddigital.com/" target="_blank">BL<span
                  class="e-flipped">E</span>ND</a></span></div>
        </div>
      </div>
    </div>
  </div>
</footer>

<div id="cookie-policy" class="position-fixed px-4 px-sm-0 cookie-policy">
  <div class="container">
    <div class="row">
      <div class="col-12 py-4 cookie-policy-content">
        <div class="text-center text-md-left d-block d-md-flex justify-content-between m-auto content">
          <p class="pb-3 pb-md-0">We care about your data, and we'd love to use cookies to make your experience better.
            For more info, view our <a href="#">cookie policy</a>.</p> <a id="accept-cookie"
            class="btn-axios btn-axios-light" href="#">accept</a>.
        </div>
      </div>
    </div>
  </div>
</div>
<div class="custom-cursor"></div>
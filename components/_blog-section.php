<div class="container articles-container">
	<div class="row">
		<div class="m-auto text-center">
			<h2 class="axios-text-light-dark underline home-white">Our Blog</h2>
		</div>
	</div>
	<div class="row pt-3 pb-4 pb-lg-5">
		<?php $the_query = new WP_Query(array('posts_per_page' => 3, 'category__not_in' => 2));?>
		<?php if ($the_query->have_posts()): ?>
		<?php $count = 0;?>
		<?php while ($the_query->have_posts()): $the_query->the_post();?>
		<?php if ($count == 0) {?>
		<div class="col-12 col-sm-12 col-md-4">
			<div class="mx-auto article-cont">
				<a href="<?php the_permalink();?>" class="text-center">
					<div class="row mx-auto article-img-cont">
						<div class="position-relative d-flex m-auto article-img-cont-in">
							<div class="mx-auto bg-img">
								<?php the_post_thumbnail('full', array('class' => 'img-fluid'));?>
							</div>
						</div>
					</div>
					<div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span>
					</div>
					<div class="row mx-auto article-text"><span class="text"><?php the_title();?></span></div>
				</a>
			</div>
		</div>

		<?php }?>
		<?php if ($count == 1) {?>
		<div class="d-none d-md-block col-12 col-sm-6 col-md-4">
			<div class="mx-auto article-cont">
				<a href="<?php the_permalink();?>" class="text-center">
					<div class="row mx-auto article-img-cont">
						<div class="position-relative d-flex m-auto article-img-cont-in">
							<div class="mx-auto bg-img">
								<?php the_post_thumbnail('full', array('class' => 'img-fluid'));?>
							</div>
						</div>
					</div>
					<div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span>
					</div>
					<div class="row mx-auto article-text"><span class="text"><?php the_title();?></span></div>
				</a>
			</div>
		</div>
		<?php }?>
		<?php if ($count == 2) {?>
		<div class="d-none d-md-block col-12 col-sm-6 col-md-4">
			<div class="mx-auto article-cont">
				<a href="<?php the_permalink();?>" class="text-center">
					<div class="row mx-auto article-img-cont">
						<div class="position-relative d-flex m-auto article-img-cont-in">
							<div class="mx-auto bg-img">
								<?php the_post_thumbnail('full', array('class' => 'img-fluid'));?>
							</div>
						</div>
					</div>
					<div class="row mx-auto article-date"><span class="m-auto pt-3 pb-2 date"><?php echo get_the_date(); ?></span>
					</div>
					<div class="row mx-auto article-text"><span class="text"><?php the_title();?></span></div>
				</a>
			</div>
		</div>
		<?php }?>

		<?php $count++;endwhile;?>
		<?php wp_reset_postdata();?>

		<?php else: ?>
		<p><?php __('No Postss');?></p>
		<?php endif;?>
		<div class="pt-5 mx-auto text-center all-articles-cont"><a class="btn-axios btn-axios-dark home-white" href="blog">all
				news</a></div>
	</div>
</div>
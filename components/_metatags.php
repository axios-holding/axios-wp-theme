<link rel="apple-touch-icon" sizes="180x180"
  href="/wp-content/themes/axios-wp-theme/assets/images/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32"
  href="/wp-content/themes/axios-wp-theme/assets/images/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16"
  href="/wp-content/themes/axios-wp-theme/assets/images/favicons/favicon-16x16.png">
<link rel="manifest" href="/wp-content/themes/axios-wp-theme/assets/images/favicons/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#0088cf">
<meta name="apple-mobile-web-app-title" content="Axios Holding Website">
<meta name="application-name" content="Axios Holding Website">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">

<?php
if (!is_tag() && !is_home() && !is_category()) {
    ?>
<link rel="canonical" href="<?php echo get_permalink(); ?>" />
<?php
}
?>

<!-- Facebook Pixel Code -->
<script>
  ! function (f, b, e, v, n, t, s) {
    if (f.fbq) return;
    n = f.fbq = function () {
      n.callMethod ?
        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
    };
    if (!f._fbq) f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = '2.0';
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s)
  }(window, document, 'script',
    'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '735986396818079');
  fbq('track', 'PageView');
</script>
<noscript>
  <img height="1" width="1" alt="" src="https://www.facebook.com/tr?id=735986396818079&ev=PageView
&noscript=1" />
</noscript>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148901774-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments)
  }
  gtag('js', new Date());

  gtag('config', 'UA-148901774-1');
</script>